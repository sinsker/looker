# Looker
`Lazy Loading Interface`<br>

**퍼포먼스 향상을 위한 지연 로딩 이벤트를 쉽게 전달 해줄수 있는 인터페이스 라이브러리입니다.**


### 1. 설치
html 코드에 해당 스크립트를 넣어 줍니다.
```javascript
 <script src="/your/path/looker.latest.js"></script>
 ```

### 2. 기본 사용법
지연 로딩를 체크할 DOM객체에 대해 해당 함수를 등록합니다.

$(object).Looker();

#### 2.1 기본 옵션
<pre>
'async': false       // 여러개의 객체가 포착 될 경우 동기화여부 { true:동기/false:비동기} 
'cointainer': window // 지연로딩에 대한 이벤트가 포착 될 컨테이너
'event': 'scroll'    // 지연로딩 이벤트

'on' : function(obj) // 객체가 포착됬을 경우의 콜백 함수
'off': function(obj) // 객체가 벗어났을 경우의 콜백 함수
'one': function(obj) // 객체가 포착됬을 경우의 최초 한번만 실행되는 콜백 함수
</pre>


### 3. 예제

```javascript
$('#obj').Looker({
 on : function(obj){
       console.log('화면에 위치합니다.');
 },
 off : function(obj){
       console.log('화면에 벗어납니다.');
 }
});
```

```javascript
$('#obj').Looker({
 one : function(obj){
    console.log('최초 한번만 실행합니다.');
 }
});
```

```javascript
$('#obj').Looker({
 async : true,
 on : function(obj){
    console.log('동기화로 실행합니다.');
 }
});
```

```javascript
$('#obj').Looker({
 event : 'click',
 one : function(obj){
    console.log('클릭시 실행합니다.');
 }
});
```
