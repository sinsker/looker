/**
 * Created by Fabbit :: 2017-04-04
 *
 *  포커스 된 객체에 대해 이벤트를 줄수 있는 인터페이스 함수
 *  Image lazyload for JQuery 를 토대로 만들었습니다.
 *
 */

(function($, window, document, undefined){

    var $window = $(window);

    var loadingFlag = false;

    $.fn.Looker = function(options) {

        var elements = this;
        var settings = {
            'async': false,
            'threshold' : 0,
            'cointainer': window,
            'event': 'scroll',

            /* 콜백 함수 */
            'on'  : null,
            'off' : null,
            'one' : null
        };

        function update() {

            var counter = 0;
            elements.each(function () {
                var $this = $(this);

                if ($.lazyup(this, settings) || $.lazydown(this, settings)) {
                    // UnLook
                    if(settings.async === true && this.loading === true){

                        this.loading = this.loading ? this.loading : false;

                        if(this.loading === true){
                            this.loading = false;
                            loadingFlag = false;
                        }
                    }
                    $this.trigger("off");

                } else if ($.inviewpoart(this, settings)) {
                    //Look
                    if(settings.async === true){

                        if(loadingFlag !== true){

                            var flag = false;
                            elements.each(function () {
                                if( this.loading === false ) {
                                    flag = true;
                                    return true;
                                }
                            });

                            if(flag !== true){
                                loadingFlag = true;
                                this.loading = this.loading ? this.loading : true;
                            }
                        }

                        if(loadingFlag === true && this.loading !== true){
                            return false;
                        }
                    }
                    $this.trigger("on");
                    counter++;
                }
            });
        }

        if (options) {
            $.extend(settings, options);
        }

        $container = (settings.container === undefined || settings.container === window) ? $window : $(settings.container);

        if (0 === settings.event.indexOf("scroll")) {
            $container.on(settings.event, function() {
                return update();
            });
        }

        this.each(function () {
            var self = this;
            var $self = $(self);

            if( self.loaded === 'undefined' ){
                self.loaded = false;
            }

            //가시영역
            $self.on("on", function () {
                if (this.loaded !== true && settings.on) {
                    settings.on($self);
                }
                self.loaded = true;
            });

            //비가시영역
            $self.on("off", function () {
                if (this.loaded === true && settings.off) {
                    settings.off($self);
                }
                self.loaded = false;
            });

            //한번만 실행
            $self.one("on", function () {
                if (this.oneloaded !== true && settings.one) {
                    settings.one($self);
                }
                self.oneloaded = true;
            });

            //scroll 이벤트가 아닐경우
            if (0 !== settings.event.indexOf("scroll")) {
                $self.on(settings.event, function() {
                    if (!self.loaded) {
                        $self.trigger("on");
                        if(!settings.one) self.loaded = false;
                    }
                });
            }
        });

        $window.on("resize", function() {
            update();
        });

        if ((/(?:iphone|ipod|ipad).*os 5/gi).test(navigator.appVersion)) {
            $window.on("pageshow", function(event) {
                if (event.originalEvent && event.originalEvent.persisted) {
                    elements.each(function() {
                        $(this).trigger("on");
                    });
                }
            });
        }

        $(document).ready(function() {
            update();
        });

    };

    $.lazyup = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerHeight ? window.innerHeight : $window.height()) + $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top + $(settings.container).height();
        }

        return fold <= $(element).offset().top - settings.threshold;
    };

    $.lazydown = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top;
        }

        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
    };

    $.inviewpoart = function(element, settings) {
        return !$.lazyup(element, settings) && !$.lazydown(element, settings);
    };

})(jQuery, window, document);